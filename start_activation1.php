<?php
include 'header.php';
?>



<div class="main">


  <div class="p-4 mb-5">


    <div class="row">
      <div class="col-md-4 mb-4">
        
        <?php
        include 'left_activation_block.php';
        ?>

      </div>
      <div class="col-md-8 mb-4">
        <div>
          <h3 class="title-color mb-0">Start Activation</h3>
          <p class="mb-0 text-info">Check Device Compatibility</p>
          <p class="mt-4">Raza Mobile work on unlocked 4G LTE phones that are compatible with VoLTE
            (Voice over LET) providers the best quality calling experience.</p>
        </div>
        <div class="row align-items-center">
          <div class="col-md-5">
            <div class="card card-brand">
              <div class="card-header">
                quick check
              </div>
              <div class="card-body">
                <p class="text-center">By brand and model</p>
                <div class="mb-3">
                  <label for="">Brand</label>
                  <select name="" id="" class="form-select">
                    <option value="">Select</option>
                    <option value="">Option 1</option>
                    <option value="">Option 2</option>
                  </select>
                </div>
                <div class="mb-3">
                  <label for="">Model</label>
                  <select name="" id="" class="form-select">
                    <option value="">Select</option>
                    <option value="">Model 1</option>
                    <option value="">Model 2</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="note px-3 mt-3">
              Results using Brand and Model are accurate for most
              phones. Use are IMEI checker for the most accurate
              results.
            </div>
          </div>
          <div class="col-md-1">
            <div class="text-center or">OR</div>
          </div>
          <div class="col-md-5">
            <div class="card card-brand">
              <div class="card-header">
                Best Results
              </div>
              <div class="card-body">
                <p class="text-center">By IMEI Number</p>
                <div class="mb-3">
                  <label for="">IMEI Number</label>
                  <input type="text" placeholder="Enter your phone’s 15-17 digit IMEI" class="form-control">
                  <div class="note mt-3 fw-normal"><i>Dail <b>#06#</b> on your phone to access IMEI</i></div>
                  <div class="mt-3">
                    <a href="#" class="note text-blue text-decoration-underline">Need Help?</a>
                  </div>

                </div>
              </div>
            </div>
            <div class="note px-3 mt-3">
              Your IMEI in your phone 10 number and lets us know if
              your phone is compatible with Raza.
            </div>
          </div>
        </div>
        <div class="form-group row">
    <div class="col-sm-20 my-5">
    <button class="btn btn-danger w-100" disabled>Continue</button>
    </div>
  </div>
      </div>

    </div>
  </div>












</div>


<?php
include 'footer.php';
?>