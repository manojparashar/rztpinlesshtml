<?php
include 'header.php';
?>



<div class="main">


  <div class="p-4 mb-5">
    <div class="row">
      <div class=" col-md-6 mb-4">
        <h3 class="title-color">Retailer Tools Box</h3>
      </div>
      <div class="col-md-6 mb-4">
        <div class="gbox1 program-phone">
          Program Your Phone
          <div class="align-self-end ps-4 float-end d-flex"><a href="#" class="fa fa-android"></a>
            <a href="#" class="fa fa-apple"></a>
            <a href="#" class="fa fa-windows"></a>
          </div>
        </div>
      </div>
    </div>


    <div class="row">
      <div class=" col-md-6 mb-4">
        <div class="gbox1">
         <div class="d-flex"> Device Compatibility Check <i class="fa fa-info"></i></div>
          <button type="button" class="btn btn-primary mt-2">Check</button>

        </div>
      </div>

      <div class="col-md-6 mb-4">
        <div class=" gbox1">
          <div class="d-flex">
          Check Zip Coverage <i class="fa fa-info"></i>
          </div>
          <div class="input-group mt-2">
            <input type="text" class="form-control" placeholder="5 Digit Zip Code" aria-label="Recipient's username" aria-describedby="basic-addon2">
            <div class="input-group-append">
              <button class="input-group-text btn-danger" id="basic-addon2">Go</button>
            </div>
          </div>
        </div>
      </div>
    </div>



    <div class="row">
      <div class=" col-md-6 mb-4">
        <div class=" gbox1">
          <div class="d-flex">Check Pin <i class="fa fa-info"></i></div>
          <div class="input-group mt-2">
            <input type="text" class="form-control" placeholder="Check Pin" aria-label="Recipient's username" aria-describedby="basic-addon2">
            <div class="input-group-append">
              <button class="input-group-text btn-danger" id="basic-addon2">Go</button>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6 mb-4">
        <div class=" gbox1">
         <div class="d-flex">Check Port Eligibility <i class="fa fa-info"></i></div>
          <div class="input-group mt-2">
            <input type="text" class="form-control" placeholder="Check Port Eligibility" aria-label="Recipient's username" aria-describedby="basic-addon2">
            <div class="input-group-append">
              <button class="input-group-text btn-danger" id="basic-addon2">Go</button>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="row">
      <div class=" col-md-6 mb-4">
        <div class=" gbox1">
          <div class="d-flex">International Rates <i class="fa fa-info"></i></div>
          <button type="button" class="btn btn-primary mt-2">Result</button>

        </div>
      </div>

      <div class="col-md-6 mb-4">
        <div class=" gbox1">
          <div class="d-flex">Call Me Free Lookup <i class="fa fa-info"></i></div>
          <div class="input-group mt-2">
            <input type="text" class="form-control" placeholder="Call Me Free Lookup" aria-label="Recipient's username" aria-describedby="basic-addon2">
            <div class="input-group-append">
              <button class="input-group-text btn-danger" id="basic-addon2">Go</button>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="row">
      <div class="col-md-6 mb-4">
        <div class=" gbox1">
          <div class="d-flex">Check IMEI <i class="fa fa-info"></i></div>
          <div class="input-group mt-2">
            <input type="text" class="form-control" placeholder="Check IMEI" aria-label="Recipient's username" aria-describedby="basic-addon2">
            <div class="input-group-append">
              <button class="input-group-text btn-danger" id="basic-addon2">Go</button>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6 mb-4">
        <div class=" gbox1">
         <div class="d-flex">Check SIM <i class="fa fa-info"></i></div>
          <div class="input-group mt-2">
            <input type="text" class="form-control" placeholder="Check SIM" aria-label="Recipient's username" aria-describedby="basic-addon2">
            <div class="input-group-append">
              <button class="input-group-text btn-danger" id="basic-addon2">Go</button>
            </div>
          </div>
        </div>
      </div>
    </div>





  </div>
</div>
<?php
include 'footer.php';
?>