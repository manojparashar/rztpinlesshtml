<?php
include 'header.php';
?>



<div class="main">
      <div class="p-4 mb-5">

            <div class="row">
                  <div class="col-md-4 mb-4 gbox1">

                  <div class="gboxdark">

<h5>Manoj Parashar<br>(872)207-4102</h5>

<div class="row my-4 fw-bold">
<div class="col ">
Current Plan:
</div>
<div class="col">
2GB | 1 Month
</div>
</div>
<div class="row">
<div class="col">
Plan Amount:
</div>
<div class="col">
$19
</div>
</div>
</div>




<div>
<div class="col-auto">
<div class="m-2 fw-bold">Status</div>

<div class="my-1">

<div class="form-control">

<div class="row">
<div class="col">
ICCID:
</div>
<div class="col">
8901260093179243077
</div>
</div>
</div>


<div class="form-control">

<div class="row">
<div class="col">
Standing:
</div>
<div class="col">
Active
</div>
</div>

</div>
<div class="form-control">

<div class="row">
<div class="col">
Account Type:
</div>
<div class="col">
Individual
</div>
</div>

</div>

<div class="form-control">

<div class="row">
<div class="col">
Payment Due<br>
Date:
</div>
<div class="col">
05/08/22<br>
11:59 PM PST
</div>
</div>

</div>
<div class="form-control">

<div class="row">
<div class="col">
Auto Recharge:
</div>
<div class="col">
Off
</div>
</div>


</div>
<div class="form-control">

<div class="row">
<div class="col">
Renewal Balance:
</div>
<div class="col">
$00.0
</div>
</div>
</div>

<div class="form-control">

<div class="row">
<div class="col">
Amount Due:
</div>
<div class="col">
$19.00
</div>
</div>
</div>
<div class="my-4">
<button class="btn btn-danger btn-block">Make a Payment</button>
</div>



<div class="col-auto">
<div class="m-2 fw-bold">Balances</div>

<div class="my-1">

<div class="form-control">

<div class="row">
<div class="col">
Wallet:
</div>
<div class="col">
$00.0
</div>
</div>
</div>

<div class="form-control">

<div class="row">
<div class="col">
INTL Dialling:
</div>
<div class="col">
$15.0
</div>
</div>
</div>

<div class="form-control">

<div class="row">
<div class="col">
uTalk:
</div>
<div class="col">
None
</div>
</div>
</div>

<div class="form-control">

<div class="row">
<div class="col">
INTL Romaing:
</div>
<div class="col">
$5.00
</div>
</div>
</div>

<div class="form-control">

<div class="row">
<div class="col">
5G + 4G LTE Data:
</div>
<div class="col fw-bold">
435.2MB left(21%)
</div>
</div>
<div class="col fw-bold my-2">
435.2MB/2GB
</div>
<div class="progress mb-2">
<div class="progress-bar bg-danger" style="width:40%"></div>
</div>
</div>


</div>
</div>
<div class="my-3">
<button class="btn btn-danger btn-block">Transation History</button>
</div>


</div>
</div>


</div>

                  </div>
                  <div class="col-md-8 mb-4">
                        <h3 class="title-color mb-0 ">Subscriber Action</h3>
                        <hr>

<div class="my-4 mx-4">

  <div class="row my-2">
    <div class="col-md-3">
          <div class="card p-3">
               
   <a href=""> <img src="./images/recharge2.png" alt="" class="mx-auto d-block"></a>
   <div class="fw-bold text-center">
         Recharge
</div>
</div>
</div>
<div class="col-md-3">
      <div class="card p-3">
<a href=""><img src="./images/load wallet.png" alt="" class="mx-auto d-block"></a>
   
   <div class="fw-bold text-center">
         Load Wallet
</div>
</div>
</div>
   <div class="col-md-3">
         <div class="card p-3">
<a href=""><img src="./images/manage plan.png" alt="" class="mx-auto d-block"></a>
   
   <div class="fw-bold text-center">
         Manage PLan
</div>
</div>
</div>
   <div class="col-md-3">
         <div class="card p-3">
<a href=""><img src="./images/auto recharge.png" alt="" class="mx-auto d-block"></a>
   
   <div class="fw-bold text-center">
         Auto Recharge
</div>
</div>
</div>
</div>


<div class="row my-2 mt-4">

  <div class="col-md-3">
        <div class="card p-3">
    
  <a href=""><img src="./images/updata.png" alt="" class="mx-auto d-block"></a>
  <div class="fw-bold text-center">
        Updata
</div>
</div>
</div>
<div class="col-md-3">
      <div class="card p-3">
<a href=""><img src="./images/upintl.png" alt="" class="mx-auto d-block"></a>
<div class="fw-bold text-center">
      Uplntl
</div>
</div>
</div>


<div class="col-md-3">
<div class="card p-3">
  <a href=""><img src="./images/world.png" alt="" class="mx-auto d-block"></a>
  <div class="fw-bold text-center">
        Globe Unlimited
</div>
</div>
</div>
<div class="col-md-3">
      <div class="card p-3">
  <a href=""><img src="./images/check.png" alt="" class="mx-auto d-block"></a>
  <div class="fw-bold text-center">
        UpRoam
</div>
</div>
</div>

</div>


<div class="row my-2 mt-4">

  <div class="col-md-3">
    <div class="card p-3">
  <a href=""><img src="./images/comment.png" alt="" class="mx-auto d-block"></a>
  <div class="fw-bold text-center">
        uTalk
</div>
</div>
</div>
<div class="col-md-3">
      <div class="card p-3">
<a href=""><img src="./images/network.png" alt="" class="mx-auto d-block"></a>
<div class="fw-bold text-center">
      Wi-Fi Talk & Text
</div>
</div>
</div>


<div class="col-md-3">
      <div class="card p-3">
  <a href=""><img src="./images/edit profile.png" alt="" class="mx-auto d-block"></a>
  <div clas="fw-bold text-center">
  <div class="fw-bold text-center">  Edit Profile</div>
</div>
</div>
</div>
<div class="col-md-3">
      <div class="card p-3">
  <a href=""><img src="./images/sim.png" alt="" class="mx-auto d-block"></a>
  <div class="fw-bold text-center">
        Sim Swap
</div>
</div>
</div>

</div>


<div class="row my-2 mt-4">

  <div class="col-md-3">
    <div class="card p-3">
  <a href=""><img src="./images/notification.png" alt="" class="mx-auto d-block"></a>
  <div class="fw-bold text-center">
        Notification
</div>
</div>
</div>
<div class="col-md-3">
      <div class="card p-1">
<a href=""><img src="./images/transaction history.png" alt="" class="mx-auto d-block"></a>
<div class="fw-bold text-center">
      Transaction <br>
      History
</div>
</div>
</div>
</div>






</div>



                  </div>
            </div>
           



      </div>
</div>
</div>












</div>


<?php
include 'footer.php';
?>