<?php
include 'header.php';
?>



<div class="main">
  <div class="p-4 mb-5">


    <div class="row">
      <div class="col-md-3 mb-4">
        <?php
        include 'left_activation_block.php';
        ?>
      </div>
      <div class="col-md-6 mb-4">
        <div>
          <h3 class="title-color pb-3">Customer Info</h3>
          <form>
            <div class="form-group d-flex">
              <label for="inputEmail3" class="col-sm-2 text-semiBold text-end me-3 fs-small col-form-label mb-5">First Name :</label>
              <div class="col-sm-10">
                <input type="First" class="form-control" id="inputEmail3" placeholder="First Name">
              </div>
            </div>
            <div class="form-group d-flex">
              <label for="inputPassword3" class="col-sm-2 text-semiBold text-end me-3 fs-small col-form-label mb-5">Last Name :</label>
              <div class="col-sm-10">
                <input type="password" class="form-control" id="inputPassword3" placeholder="Last Name">
              </div>
            </div>
            <div class="form-group d-flex">
              <label for="inputPassword3" class="col-sm-2 text-semiBold text-end me-3 fs-small col-form-label mb-5">Email :</label>
              <div class="col-sm-10">
                <input type="password" class="form-control" id="inputPassword3" placeholder="Email">
              </div>
            </div>


            <div class="form-group row float-sm-end">
              <div class="col-sm-20">
                <button type="submit" class="btn btn-danger">Continue</button>
              </div>
            </div>
          </form>
        </div>
      </div>

      <div class="col-md-3 mb-4">
        <div class="boxn mb-4">
          <h5 class="title-color">Selected Plan</h5>
          <div class="row mb-2">
            <div class="col text-primary">
              Name:
            </div>
            <div class="col">
              250 MB | 1 Month
            </div>
          </div>
          <div class="row">
            <div class="col text-primary">
              Plan Amount:
            </div>
            <div class="col fs-5 fw-bold">
              $ 15.00
            </div>
          </div>
        </div>
        <div class="boxn mb-4">
          <h5 class="title-color">Activation Status</h5>
          <div class="row mb-2">
            <div class="col text-primary">
              ICCID:
            </div>
            <div class="col">
              8901260093179242194
            </div>
          </div>
          <div class="row mb-2">
            <div class="col text-primary">
              Activation Type:
            </div>
            <div class="col">
              NEW
            </div>
          </div>
          <div class="row mb-2">
            <div class="col text-primary">
              Phone Numner:
            </div>
            <div class="col">
              N/A
            </div>
          </div>
          <div class="row mb-2">
            <div class="col text-primary">
              Created:
            </div>
            <div class="col">
              02/21/2022 10:50 am
            </div>
          </div>
          <div class="row mb-2">
            <div class="col text-primary">
              Status:
            </div>
            <div class="col">
              Neutral
            </div>
          </div>
        </div>
      </div>


    </div>
  </div>

</div>


<?php
include 'footer.php';
?>