<?php
include 'header.php';
?>



<div class="main">
    <div class="p-4 mb-5">
        <div class="banner">
            <div class="banner-strip">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus repellendus, provident libero dolores veritatis eaque accusamus? Vel quidem necessitatibus a veritatis debitis quibusdam, maxime, ullam ipsum tempore repudiandae corrupti expedita?
            </div>
            <img src="./images/add_banner.png" alt="banner" class="img-fluid w-100">
        </div>
        <h3 class="title-color pt-sm-4">Dashborad</h3>
        <div class="row">
            <div class="col-md-4 mb-4">
                <div class="box text-center">
                    <h6 class="text-danger">2ND MONTH RECHARGE</h6>
                    <div class="range">53%</div>
                    <p class="small text-medium">JAN 2022 (COMPLETED)</p>
                    <hr class="divider">
                    <div class="range">53%</div>
                    <p class="small text-medium">JAN 2022 (COMPLETED)</p>
                    <button class="btn btn-danger rounded-pill">Expiring Subscribers <i class="fa fa-chevron-right ps-2"></i></button>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="box text-center">
                    <h6 class="text-danger">ACTIVATION TRACKER</h6>
                    <img src="./images/graph.png" alt="graph" class="img-fluid">
                </div>
            </div>

            <div class="col-md-4 mb-4">
                <div class="box">
                    <h6 class="text-danger mb-0 text-center">DEMO LINE 1 <i class="fa fa-info-circle text-danger" aria-hidden="true"></i></h6>
                    <p class="small text-medium text-center">(UPDATED NIGHTLY)</p>
                    <p class="small text-medium">MSISDN: <a href="#">(872) 806-8637</a></p>
                    <div class="d-flex justify-content-between align-items-center">
                        <p class="small text-medium mb-0">Status: <span class="text-gray">Active</span></p>
                        <button class="btn btn-secondary rounded-pill" disabled>Claimed</button>
                    </div>
                    <div class="progress my-3">
                        <div class="progress-bar w-100" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <h4 class="small text-medium text-center">10 / 10</h4>

                </div>
            </div>

            <div class="col-md-4 mb-4">
                <div class="box">
                    <h6 class="text-danger mb-0 text-center">PLAN STATUSES</h6>
                    <div class="d-flex justify-content-between align-items-center border-bottom mt-3 pb-3">
                        <p class="small text-medium mb-0">Active</p>
                        <p class="small text-medium mb-0">803</p>
                    </div>
                    <div class="d-flex justify-content-between align-items-center border-bottom mt-3 pb-3">
                        <p class="small text-medium mb-0">Active</p>
                        <p class="small text-medium mb-0">803</p>
                    </div>
                    <div class="d-flex justify-content-between align-items-center border-bottom mt-3 pb-3">
                        <p class="small text-medium mb-0">Active</p>
                        <p class="small text-medium mb-0">803</p>
                    </div>
                    <div class="d-flex justify-content-between align-items-center border-bottom mt-3 pb-3">
                        <p class="small text-medium mb-0">Active</p>
                        <p class="small text-medium mb-0">129</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="box text-center">
                    <h6 class="text-danger">TOTAL ACTIVATOPMS: 39</h6>
                    <img src="./images/circle_chart.png" alt="graph" class="img-fluid">
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="box">
                    <h6 class="text-danger mb-0 text-center">DEMO LINE 1 <i class="fa fa-info-circle text-danger" aria-hidden="true"></i></h6>
                    <p class="small text-medium text-center">(UPDATED NIGHTLY)</p>
                    <p class="small text-medium">MSISDN: <a href="#">(872) 806-8637</a></p>
                    <div class="d-flex justify-content-between align-items-center">
                        <p class="small text-medium mb-0">Status: <span class="text-gray">Active</span></p>
                        <button class="btn btn-secondary rounded-pill" disabled>Claimed</button>
                    </div>
                    <div class="progress my-3">
                        <div class="progress-bar w-100" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <h4 class="small text-medium text-center">10 / 10</h4>

                </div>
            </div>
        </div>
        <h3 class="title-color pt-sm-4">Latest Update From Raza</h3>
            <hr>
            <h5 class="blueText pt-sm-3">Ultra Mobile Retailer Activate-April 2022</h5>
            <span class="text-darkgray">04/01/22</span>
            <h5 class="blueText pt-sm-3">Ultra Mobile Retailer Activate - March 2022</h5>
            <span class="text-darkgray">02/28/22</span>
            <h5 class="blueText pt-sm-3">Ultra Mobile Unlimited for just $24/mo!</h5>
            <span class="text-darkgray">02/28/22</span>
            <p>UPDATED FAQs! Add up to 4 additional lines of the $49 Unlimited 1-Month Plan for just $24 a month each! Level Up in 2022 with Ultra Mobile’s BEST UNLIMITED OFFER EVER. And Learn HOW to Activate Add-a-Line!</p>
            <h5 class="blueText pt-sm-3">Happy New Year! - Jan 2022 Activate</h5>
            <span class="text-darkgray">12/31/21</span>
            <p>We want to give all our amazing Ultra Retailers a huge Ultra-thank you for your work and dedication throughout 2021.</p>
            <h5 class="blueText pt-sm-3">Ultra Mobile retailer Activate - November 2021</h5>
            <span class="text-darkgray">12/31/21</span>
            <h5 class="blueText pt-sm-3">Happy New Year! - Jan 2022 Activate</h5>
            <span class="text-darkgray">12/31/21</span>
            <p>Find out what's new at Ultra Mobile this October!</p>
            <h5 class="blueText pt-sm-3">Happy New Year! - Jan 2022 Activate</h5>
            <span class="text-darkgray">12/31/21</span>
            <p>We've Extended our Customer FREE MONTH Promotion AND our Retailer Bonus Incentive</p>
            <h5 class="blueText pt-sm-3">Happy New Year! - Jan 2022 Activate</h5>
            <span class="text-darkgray">12/31/21</span>
            <p>Find out what's new at Ultra Mobile this October!</p>
            <h5 class="blueText pt-sm-3">Happy New Year! - Jan 2022 Activate</h5>
            <span class="text-darkgray">12/31/21</span>
            <p>We've Extended our Customer FREE MONTH Promotion AND our Retailer Bonus Incentive</p>
            
    </div>
    

</div>








<?php
include 'footer.php';
?>