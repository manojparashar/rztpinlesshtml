<?php
    include 'header.php';
?>


<div class="popup popup-success">
    <div class="d-flex h-100 align-items-center">
        <div class="icon-wrapper">
        <img src="images/ok.png" alt="ok">
        </div>
        <div class="popup-content">
        <h2>Activation Successful</h2>
            <p>The Subscriber pin has been successfully activated</p>
            <div class="text-end">
                <button class="btn-black">OK</button>
            </div>
        </div>
    </div>
</div>














<?php
    include 'footer.php';
?>