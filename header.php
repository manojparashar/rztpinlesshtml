<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <link rel="stylesheet" type="text/css" href="css/menu.css">
    <link rel="stylesheet" type="text/css" href="css/font-face.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha512-SfTiTlX6kk+qitfevl/7LibUOeJWlt9rbyDn92a1DqWOw9vWG2MFoays0sgObmWazO5BQPiFucnnEAjpAB+/Sw==" crossorigin="anonymous" referrerpolicy="no-referrer" />

</head>
<header>
    <div class="header-strip d-none d-md-block">
        <ul>
            <li><a href="#">raza recharge</a></li>
            <li><a href="#">International top up</a></li>
            <li class="active"><a href="#">Raza 5g</a></li>
            <li><a href="#">Domestic top up</a></li>
        </ul>
    </div>

    <div class="fixed-top-head innerTopHead">
        <div class="w-100 pull-right position-relative d-none d-md-block">
            <div class="row">
                <ul class="d-lg-flex d-block align-items-center">
                    <li class="me-auto">
                        <h5><label class="call_numb">
                        Customer Support : 1-877-634-2782<br>
                                Retailer Support : 1-877-634-2782
                                
                            </label></h5>
                    </li>
                    <!-- <li>
                        <h5>Retailer : <label id="lblStoreName">Carchicago</label></h5>
                    </li> -->
                    <!-- <li id="bal_mob" class="pe-4">
                        <h5><span>Balance:</span> $42</h5>
                    </li> -->
                    <li>
                        <h5><label class="me-4 store-details">
                        Store Name : Raza Etobicoke<br>
                                Account # : 1234567890
                                
                            </label></h5>
                    </li>
                    <li id="ad_bal" class="pe-4"><a class="addcredit-btn " href="#">Balance <span>$550</span></a></li>
                </ul>
            </div>
        </div>
    </div>
</header>
<div class="btn-group">
  <button type="button" class="btn btn-danger dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
    Action
  </button>
  <ul class="dropdown-menu">
    <li><a class="dropdown-item" href="#">Action</a></li>
    <li><a class="dropdown-item" href="#">Another action</a></li>
    <li><a class="dropdown-item" href="#">Something else here</a></li>
    <li><hr class="dropdown-divider"></li>
    <li><a class="dropdown-item" href="#">Separated link</a></li>
  </ul>
</div>

<div class="sidenav inner-sidenav-mobile" id="collapseclick">
    <div class="mobileLogo">
        <a class="sidenav-logo" href="createManageAccount.aspx"><img src="./images/logo.png" alt="Raza Logo"></a>
    </div>

    <!--<div class="inner-mobile-logo"> 
	<a class="sidenav-logo" href="javascript:void(0)">
	<img src="./images/logo.png" alt="Raza Logo"></a>
    
  </div>-->

    <nav class="inner-pushmenu inner-pushmenu-left">
    
        <ul class="inner-links">
            <li class="RazaNumber d-block d-md-none"><a href="create_manage_account_with_phonecard.aspx">
            Customer Support : 1-877-634-2782<br>
            Retailer Support : 1-877-634-2782
            </a></li>

            <li class="RazaBalance mb-3 d-block d-md-none">
            <span class="mb-3 text-center d-block">Balance : $550</span>
            <a class="addcredit-btn " href="#">Balance <span>$550</span></a>
            </li>

            <!--<li class="mobileLogo"><a class="sidenav-logo" href="index.aspx"><img src="./images/logo.png" alt="Raza Logo" /></a></li>-->

            <!--<li class="mobileLogo"><a class="sidenav-logo" href="index.aspx"><img src="./images/logo.png" alt="Raza Logo"></a></li>-->
            <li>
                <a class="support-nav" href="javascript:void(0)"><span class="spriteIimage resource-icon"></span>Resources<img src="./images/down-arrow-support.png" alt="down arrow"></a>
                <ul class="sub-menu" style="display:none;">
                    <li><a class="menu-title ms-2" href="javascript:void(0)">Marketing</a>
                    <li><a href="ActivateNewsletter.php"><span class="spriteIimage currentBalance-icon"></span>Activate Newsletter</a></li>
                    <li>
                        <a class="setting-nav retailer_profile" href="javascript:void(0)"><span class="spriteIimage currentBalance-icon"></span>Retailer Benefits<img src="./images/down-arrow-support.png" alt="down arrow"></a>

                        <ul class="sub-menu2" style="display:none;">
                            <li><a href="DemoLineProgram.php"><span class="spriteIimage useProfile-icon"></span>Demo Lines</a></li>
                            <li><a href="RazaTV.php"><span class="spriteIimage changePassword-icon"></span> Raza TV</a>

                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="setting-nav plan" href="javascript:void(0)"><span class="spriteIimage currentBalance-icon"></span>Plans & Features<img src="./images/down-arrow-support.png" alt="down arrow"></a>

                        <ul class="sub-menu3" style="display:none;">
                            <li><a href="rztpinless_Brochures.php"><span class="spriteIimage useProfile-icon"></span>Brochures</a></li>
                            <li><a href="rztpinless_Features.php"><span class="spriteIimage changePassword-icon"></span>Features</a>

                            </li>
                        </ul>
                    </li>
                    <li><a href="rztpinless_Compensation.php"><span class="spriteIimage currentBalance-icon"></span>Compensation</a></li>
                    <li><a href="rztpinless_MerchandisingBrandUsage.php"><span class="spriteIimage currentBalance-icon"></span>Merchandising & Brand</a></li>
                    <li>
                        <a class="setting-nav print" href="javascript:void(0)"><span class="spriteIimage currentBalance-icon"></span>Print Ready Collateral<img src="./images/down-arrow-support.png" alt="down arrow"></a>

                        <ul class="sub-menu4" style="display:none;">
                            <li><a href="rztpinless_22_28Posters.php"><span class="spriteIimage useProfile-icon"></span>22x28</a></li>
                            <li><a href="rztpinless_11_28.php"><span class="spriteIimage changePassword-icon"></span> 11x28</a>
                            <li><a href="inserts.php"><span class="spriteIimage changePassword-icon"></span> Inserts</a>

                            </li>
                        </ul>
                    </li>
                    <li><a href="rztpinless_UnlimitedCountries.php"><span class="spriteIimage currentBalance-icon"></span>Unlimited Countries</a></li>
                    <li><a class="menu-title ms-2" href="javascript:void(0)">TRAINING</a>
                    <li><a href="rztpinless_faqs.php"><span class="spriteIimage changePassword-icon"></span> FAQs</a>
                    <li><a href="handsetconfiguration.php"><span class="spriteIimage changePassword-icon"></span> Handset Configuration</a>
                    <li><a href="rztpinless_videos.php"><span class="spriteIimage changePassword-icon"></span> Videos</a>
                    <li><a href="search_resources.php"><span class="spriteIimage changePassword-icon"></span> Search</a>
                </ul>
            <li><a href="start_newactivationportin.php"><span class="spriteIimage activation-icon"></span>Activations</a></li>
            <li><a href="manage_subscriber.php"><span class="spriteIimage managesub-icon"></span>Manage Subcriber</a></li>
            <li><a href="retailer_tools.php"><span class="spriteIimage toolbox-icon"></span>Tool Box</a></li>
            <li class="RzaPhneCrd"><a href="create_manage_account_with_phonecard.aspx"><span class="spriteIimage razaphonecard-icon"></span>Raza Phone Card</a></li>
            <li class="RzaPhneCrd"><a href="video_library.aspx"><span class="spriteIimage videolibrary-icon"></span>Video Library<span class="spriteIimage videolibrary-icon1"></span></a></li>

        </ul>
    </nav>

    <div id="inner-nav_list">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
    </div>

</div>