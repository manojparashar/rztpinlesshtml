<?php
include 'header.php';
?>



<div class="main">

  <div class="p-4 mb-5">
    <div class="row">
      <div class="col-md-12 mb-4 search-details">
        <h3 class="title-color mb-0 "> Search Results For: <span>(773) 410-4351</span> <a href="#">Clear</a></h3>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 mb-2">
        <div class="gbox1 ">
          <p class="text-semiBold">Subscriber Snapshot</p>
          <div class="card">
            <table class="table table-small result-table mb-0">

              <tbody>
                <tr>
                  <td class="text-semiBold p-2">Number</td>
                  <td class="text-start">(773) 410-4351</td>
                </tr>
                <tr>
                  <td class="text-semiBold p-2">Plan</td>
                  <td class="text-start">250 MB | 1 Month</td>
                </tr>
                <tr>
                  <td class="text-semiBold p-2">Created</td>
                  <td class="text-start">02/21/2022</td>

                </tr>
                <tr>
                  <td class="text-semiBold p-2">Account Type</td>
                  <td class="text-start">Individual</td>

                </tr>
                <tr>
                  <td class="text-semiBold p-2">Auto Recharge</td>
                  <td class="text-start">Off</td>

                </tr>
                <tr>
                  <td class="text-semiBold p-2">Status</td>
                  <td class="text-start">Provisioned</td>

                </tr>
                <tr>
                  <td class="text-semiBold p-2">Amout Due</td>
                  <td class="text-start">$15.00</td>

                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="text-center">
      <button type="button" class="btn btn-danger mt-2">Add Fund</button>

    </div>
  </div>
</div>
</div>
<?php
include 'footer.php';
?>