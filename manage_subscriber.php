<?php
include 'header.php';
?>



<div class="main">
  <div class="p-4 mb-5">
    <div class="row">
      <div class="col-md-12 mb-4">
        <h3 class="title-color mb-0"> Manage Subscriber</h3>
      </div>
    </div>

    <div class="gbox1">
      Find Any subscriber
      <div class="row">
        <div class="col-6">
          <div class="input-group mt-2">
            <input type="text" class="form-control" placeholder="MSISDN" aria-label="Recipient's username" aria-describedby="basic-addon2">
            <div class="input-group-append">
              <button class="input-group-text btn-danger" id="basic-addon2">Go</button>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="input-group mt-2">
            <input type="text" class="form-control" placeholder="ICCID" aria-label="Recipient's username" aria-describedby="basic-addon2">
            <div class="input-group-append">
              <button class="input-group-text btn-danger" id="basic-addon2">Go</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="gbox1 my-4">
      <div class="row">
        <div class="col-6">
          Search Subscribers by contact Info
          <div class="row my-2">
            <div class="col-md-6">
              <input type="text" placeholder="First Name" class="form-control">
            </div>
            <div class="col-md-6">
              <input type="text" placeholder="Last Name" class="form-control">
            </div>

          </div>

          <div class="row my-2">
            <div class="col-md-6">
              <input type="text" placeholder="Email" class="form-control">
            </div>
            <div class="col-md-6">
              <input type="text" placeholder="Zip" class="form-control">
            </div>
          </div>

          <div class="row my-2">
            <div class="col-md-6">
              <button class="btn btn-danger w-100"> Go </button>
            </div>
            <div class="col-md-6">
              <button class="btn btn-secondary w-100">Reset </button>
            </div>
          </div>




        </div>
        <div class="col-6">
          Search subscribers by Plan Type
          <div class="card mt-2">
            <div class="accordion" id="accordionExample">
              <div class="accordion-item">
                <h2 class="accordion-header" id="headingOne">
                  <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Active
                  </button>
                </h2>
                <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                  <div class="accordion-body">
                    <P>Select the <b>Plan Started On</b> date range:</p>
                    <div class="row my-2">
                      <div class="col-4">
                        <select name="Last 24 Hours" id="" class="form-select">
                          <option value="">Last 24 Hours</option>
                          <option value="">3 Days</option>
                          <option value="">7 Days</option>
                          <option value="">30 Days</option>
                          <option value="">Custom</option>
                        </select>
                      </div>
                      <div class="col-4">
                        <button type="button" class="btn btn-danger btn-block">Search</button>
                      </div>

                    </div>

                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="headingTwo">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Port-In Attempted
                  </button>
                </h2>
                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                  <div class="accordion-body">
                    <P>Select the <b>Port Status Updated</b> date range:</p>
                    <div class="row my-2">
                      <div class="col-4">
                        <select name="Last 24 Hours" id="" class="form-select">
                          <option value="">Last 24 Hours</option>
                          <option value="">3 Days</option>
                          <option value="">7 Days</option>
                          <option value="">30 Days</option>
                          <option value="">Custom</option>
                        </select>
                      </div>
                      <div class="col-4">
                        <button type="button" class="btn btn-danger btn-block">Search</button>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="headingThree">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    Provisioned
                  </button>
                </h2>
                <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                  <div class="accordion-body">
                    <P>Select the <b>User Created On</b> date range:</p>
                    <div class="row my-2">
                      <div class="col-4">
                        <select name="Last 24 Hours" id="" class="form-select">
                          <option value="">Last 24 Hours</option>
                          <option value="">3 Days</option>
                          <option value="">7 Days</option>
                          <option value="">30 Days</option>
                          <option value="">Custom</option>
                        </select>
                      </div>
                      <div class="col-4">
                        <button type="button" class="btn btn-danger btn-block">Search</button>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="headingFour">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                    Suspended
                  </button>
                </h2>
                <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                  <div class="accordion-body">
                    <P>Select the <b>Plan Expired On</b> date range:</p>
                    <div class="row my-2">
                      <div class="col-4">
                        <select name="Last 24 Hours" id="" class="form-select">
                          <option value="">Last 24 Hours</option>
                          <option value="">3 Days</option>
                          <option value="">7 Days</option>
                          <option value="">30 Days</option>
                          <option value="">Custom</option>
                        </select>
                      </div>
                      <div class="col-4">
                        <button type="button" class="btn btn-danger btn-block">Search</button>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="headingFive">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                    Cancelled
                  </button>
                </h2>
                <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#accordionExample">
                  <div class="accordion-body">
                    <P>Select the <b>Deactivated On</b> date range:</p>
                    <div class="row my-2">
                      <div class="col-4">
                        <select name="Last 24 Hours" id="" class="form-select">
                          <option value="">Last 24 Hours</option>
                          <option value="">3 Days</option>
                          <option value="">7 Days</option>
                          <option value="">30 Days</option>
                          <option value="">Custom</option>
                        </select>
                      </div>
                      <div class="col-4">
                        <button type="button" class="btn btn-danger btn-block">Search</button>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="headingSix">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                    Expiring Subscribers
                  </button>
                </h2>
                <div id="collapseSix" class="accordion-collapse collapse" aria-labelledby="headingSix" data-bs-parent="#accordionExample">
                  <div class="accordion-body">
                    <div class="row my-2">
                      <div class="col-12">
                        <button type="button" class="btn btn-danger btn-block">Search</button>
                      </div>
                    </div>

                  </div>
                </div>
              </div>



            </div>
          </div>
        </div>
      </div>
    </div>

  </div>







</div>
</div>
<?php
include 'footer.php';
?>