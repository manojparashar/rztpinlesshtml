<?php
    include 'header.php';
?>


<div class="popup popup-warning">
    <div class="d-flex h-100 align-items-center">
        <div class="icon-wrapper">
        <img src="images/warning.png" alt="ok">
        </div>
        <div class="popup-content">
            <h2>Error</h2>
            <p>The Subscriber number does not match the our records for your account.</p>
            <div class="text-end">
                <button class="btn-black">OK</button>
            </div>
        </div>
    </div>
</div>














<?php
    include 'footer.php';
?>