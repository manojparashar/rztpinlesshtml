<?php
include 'header.php';
?>



<div class="main">
  <div class="p-4 mb-5">


    <div class="row">
      <div class="col-md-3 mb-4">
        <?php
        include 'left_activation_block.php';
        ?>
      </div>
      <div class="col-md-6 mb-4">
        <div class="gbox1">
          <h5 class="title-color">Important</h5>
          <p class="fs-small">Notify customer that if they are planing to transfer their current phone
            number to Raza mobile to make sure to get the following information
            directly from there current wireless carrier. Account Number, PIN/passcode,
            and zip code associated with their account.</p>

          <p class="fs-small mb-0">Any incorrect information...</p>
        </div>
        <div class="my-sm-4">

          <form class="custom-form">
            <div class="form-group row">
              <label for="inputEmail3" class="fs-small text-semiBold text-start col-sm-3 col-form-label mb-5">Phone
                Number:</label>
              <div class="col-sm-9">
                <input type="First" class="form-control" id="inputEmail3" placeholder="Required">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="fs-small text-semiBold text-start col-sm-3 col-form-label mb-5">Accounts:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="inputPassword3" placeholder="Required (alphanumeric only)">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="fs-small text-semiBold text-start col-sm-3 col-form-label mb-5">Password:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="inputPassword3" placeholder="Required (alphanumeric only)">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="fs-small text-semiBold text-start col-sm-3 col-form-label mb-5">Zip code:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="inputPassword3" placeholder="Required (numeric only)">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="fs-small text-semiBold text-start col-sm-3 col-form-label mb-5">Alternate phone Number:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="inputPassword3" placeholder="Optional">
              </div>
            </div>


            <div class="form-group row float-sm-end">
              <div class="col-sm-20">
                <a href="#" class="undeline-primary me-sm-4">Goback</a>
                <button type="submit" class="btn btn-danger">Continue</button>
              </div>
            </div>
          </form>
        </div>
      </div>

      <div class="col-md-3 mb-4">
        <div class="boxn mb-4">
          <h5 class="title-color">Selected Plan</h5>
          <div class="row mb-2">
            <div class="col text-primary">
              Name:
            </div>
            <div class="col">
              250 MB | 1 Month
            </div>
          </div>
          <div class="row">
            <div class="col text-primary">
              Plan Amount:
            </div>
            <div class="col fs-5 fw-bold">
              $ 15.00
            </div>
          </div>
        </div>
        <div class="boxn mb-4">
          <h5 class="title-color">Activation Status</h5>
          <div class="row mb-2">
            <div class="col text-primary">
              ICCID:
            </div>
            <div class="col">
              8901260093179242194
            </div>
          </div>
          <div class="row mb-2">
            <div class="col text-primary">
              Activation Type:
            </div>
            <div class="col">
              NEW
            </div>
          </div>
          <div class="row mb-2">
            <div class="col text-primary">
              Phone Numner:
            </div>
            <div class="col">
              N/A
            </div>
          </div>
          <div class="row mb-2">
            <div class="col text-primary">
              Created:
            </div>
            <div class="col">
              02/21/2022 10:50 am
            </div>
          </div>
          <div class="row mb-2">
            <div class="col text-primary">
              Status:
            </div>
            <div class="col">
              Neutral
            </div>
          </div>
        </div>
      </div>


    </div>
  </div>

</div>


<?php
include 'footer.php';
?>