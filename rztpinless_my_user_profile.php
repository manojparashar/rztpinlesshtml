<?php
include 'header.php';
?>



<div class="main">
    <div class="p-4 mb-5">
    <h3 class="title-color py-3 mb-0">Edit My User Profile :</h3>
    <hr>

<div class="col-md-4 mb-4 text-success">
    Basic Info
</div>

<div class="row fw-bold">
<div class=" col-md-4 mb-4">

 First Name
 <div class="input-group mt-2">
  <input type="text" class="form-control" placeholder="First Name" aria-label="Recipient's username" aria-describedby="basic-addon2">
  
</div>
</div>


<div class="col-md-4 mb-4">

  Last Name 
  <div class="input-group mt-2">
  <input type="text" class="form-control" placeholder="Last Name" aria-label="Recipient's username" aria-describedby="basic-addon2">
 
</div>
</div>
</div>


<div class="row fw-bold">
<div class=" col-md-4 mb-4">

 Username
 <div class="input-group mt-2">
  <input type="text" class="form-control" placeholder="username" aria-label="Recipient's username" aria-describedby="basic-addon2">
  
</div>
</div>
<div class="col-md-4 mb-4">

  Email  
  <div class="input-group mt-2">
  <input type="text" class="form-control" placeholder= "eg. xyz12@gmail.com" arial-label="Recipient's username" aria-describedby="basic-addon2">
 
</div>
</div>
</div>


<div class="col-md-4 mb-4 fw-bold">

  Phone Number
  <div class="input-group mt-2">
  <input type="text" class="form-control" placeholder= "phone number" arial-label="Recipient's username" aria-describedby="basic-addon2">
 
</div>
</div>


<div class="col-md-4 mb-4 text-success">
    Update Password (Optional)
</div>



<div class="row fw-bold">
<div class=" col-md-4 mb-4">

 New Password
 <div class="input-group mt-2">
  <input type="text" class="form-control" placeholder="" aria-label="Recipient's username" aria-describedby="basic-addon2">
  
</div>
</div>
<div class="col-md-4 mb-4">

  Confirm Password
  <div class="input-group mt-2">
  <input type="text" class="form-control" placeholder= "" arial-label="Recipient's username" aria-describedby="basic-addon2">
 
</div>
</div>
</div>

<div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
  <label class="form-check-label" for="flexCheckIndeterminate">
    Show Password
  </label>
  
</div>
<div>
<span class="btn btn-danger mt-4" id="">SAVE INFO</span>
</div>




</div>
</div>
<?php
include 'footer.php';
?>