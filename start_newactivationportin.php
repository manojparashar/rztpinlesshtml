<?php
include 'header.php';
?>



<div class="main">
      <div class="p-4 mb-5">

            <div class="row">
                  <div class="col-md-4 mb-4">

                        <?php
                        include 'left_activation_block.php';
                        ?>

                  </div>
                  <div class="col-md-8 mb-4">
                        <h3 class="title-color mb-0 ">Start New Activation</h3>
                        <div class=" gbox1 mt-3 rounded-3">
                              Please enter the SIM card’s 19-digit ICCID number:
                              <div class="input-group mt-2">
                                    <input type="text" class="form-control" placeholder="Enter the ICCID number to begin." aria-label="Recipient's username" aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                          <button class="input-group-text btn-danger" id="basic-addon2">Go</button>
                                    </div>
                              </div>
                        </div>
                  </div>
            </div>
            <div class="gbox1 mt-3 rounded-3 mb-3">
                  <div class="row">
                        <div class="col-md-6">
                              <div class="row my-2">
                                    <div class="col-md-4 mb-3 mb-lg-0">
                                          <select name="" id="" class="form-select">
                                                <option value="">Month to Date</option>
                                                <option value="">Option 1</option>
                                                <option value="">Option 2</option>
                                          </select>
                                    </div>
                                    <div class="col-md-2 mb-3 mb-lg-0">
                                          <button class="btn btn-danger"> Refresh </button>
                                    </div>
                                    <div class="col-md-4 mb-3 mb-lg-0">
                                          <input type="text" placeholder="Filter Result" class="form-control">
                                    </div>

                              </div>
                        </div>
                  </div>
                  <div class="row">
                        <div class="col-md-12 mt-3 small">
                              <table class="table table-bordered table-responsive portin-table bg-white">
                                    <thead class="table-head-bg">
                                          <tr>
                                                <th>ICCID</th>
                                                <th>MSISDN</th>
                                                <th>Name</th>
                                                <th>Plan</th>
                                                <th>Type</th>
                                                <th>Status</th>
                                                <th>Started</th>
                                                <th>Updated</th>
                                                <th>Funded</th>
                                                <th>Message</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                          <tr>
                                                <td>8901260093179242194</td>
                                                <td>(773) 410-4351</td>
                                                <td>tousifahmed
                                                      hassan</td>
                                                <td>250MB | 1 Month</td>
                                                <td>PORT</td>
                                                <td><a href="#">Resolution
                                                            Required</a></td>
                                                <td>03/10/2022
                                                      09:52 am
                                                      PST</td>
                                                <td>03/10/2022
                                                      09:52 am
                                                      PST</td>
                                                <td><a href="#">Add Funds</a></td>
                                                <td>invalidPin
                                                      <a href="#">Update Port</a>
                                                </td>
                                          </tr>
                                          <tr>
                                                <td>8901260093179242194</td>
                                                <td><a href="#">(773) 410-4351</a></td>
                                                <td>tousifahmed
                                                      hassan</td>
                                                <td>250MB | 1 Month</td>
                                                <td>PORT</td>
                                                <td>Provisioned</td>
                                                <td>03/10/2022
                                                      09:52 am
                                                      PST</td>
                                                <td>03/10/2022
                                                      09:52 am
                                                      PST</td>
                                                <td>Add Funds</td>
                                                <td>invalidPin
                                                      <a href="#">Update Port</a>
                                                </td>
                                          </tr>
                                          <tr>
                                                <td>8901260093179242194</td>
                                                <td><a href="#">(773) 410-4351</a></td>
                                                <td>tousifahmed
                                                      hassan</td>
                                                <td>250MB | 1 Month</td>
                                                <td>PORT</td>
                                                <td>Completed</td>
                                                <td>03/10/2022
                                                      09:52 am
                                                      PST</td>
                                                <td>03/10/2022
                                                      09:52 am
                                                      PST</td>
                                                <td>$49</td>
                                                <td>invalidPin
                                                      <a href="#">Update Port</a>
                                                </td>
                                          </tr>
                                          <tr>
                                                <td>8901260093179242194</td>
                                                <td><a href="#">(773) 410-4351</a></td>
                                                <td>tousifahmed
                                                      hassan</td>
                                                <td>250MB | 1 Month</td>
                                                <td>PORT</td>
                                                <td>Completed</td>
                                                <td>03/10/2022
                                                      09:52 am
                                                      PST</td>
                                                <td>03/10/2022
                                                      09:52 am
                                                      PST</td>
                                                <td>Add Funds</td>
                                                <td>invalidPin
                                                      <a href="#">Update Port</a>
                                                </td>
                                          </tr>
                                          <tr>
                                                <td>8901260093179242194</td>
                                                <td><a href="#">(773) 410-4351</a></td>
                                                <td>tousifahmed
                                                      hassan</td>
                                                <td>250MB | 1 Month</td>
                                                <td>PORT</td>
                                                <td>Completed</td>
                                                <td>03/10/2022
                                                      09:52 am
                                                      PST</td>
                                                <td>03/10/2022
                                                      09:52 am
                                                      PST</td>
                                                <td>Add Funds</td>
                                                <td>invalidPin
                                                      <a href="#">Update Port</a>
                                                </td>
                                          </tr>
                                          <tr>
                                                <td>8901260093179242194</td>
                                                <td><a href="#">(773) 410-4351</a></td>
                                                <td>tousifahmed
                                                      hassan</td>
                                                <td>250MB | 1 Month</td>
                                                <td>PORT</td>
                                                <td>Completed</td>
                                                <td>03/10/2022
                                                      09:52 am
                                                      PST</td>
                                                <td>03/10/2022
                                                      09:52 am
                                                      PST</td>
                                                <td>Add Funds</td>
                                                <td>invalidPin
                                                      <a href="#">Update Port</a>
                                                </td>
                                          </tr>
                                          <tr>
                                                <td>8901260093179242194</td>
                                                <td><a href="#">(773) 410-4351</a></td>
                                                <td>tousifahmed
                                                      hassan</td>
                                                <td>250MB | 1 Month</td>
                                                <td>PORT</td>
                                                <td>Completed</td>
                                                <td>03/10/2022
                                                      09:52 am
                                                      PST</td>
                                                <td>03/10/2022
                                                      09:52 am
                                                      PST</td>
                                                <td>Add Funds</td>
                                                <td>invalidPin
                                                      <a href="#">Update Port</a>
                                                </td>
                                          </tr>
                                          <tr>
                                                <td>8901260093179242194</td>
                                                <td><a href="#">(773) 410-4351</a></td>
                                                <td>tousifahmed
                                                      hassan</td>
                                                <td>250MB | 1 Month</td>
                                                <td>PORT</td>
                                                <td>Completed</td>
                                                <td>03/10/2022
                                                      09:52 am
                                                      PST</td>
                                                <td>03/10/2022
                                                      09:52 am
                                                      PST</td>
                                                <td>Add Funds</td>
                                                <td>invalidPin
                                                      <a href="#">Update Port</a>
                                                </td>
                                          </tr>
                                    </tbody>
                              </table>
                              <div class="row align-items-center">
                                    <div class="col-md-3">
                                          <button type="button" class="btn btn-danger">CSV</button>
                                          <button type="button" class="btn btn-danger">Excel</button>
                                    </div>
                                    <div class="col-md-6">
                                          <div class="text-center">Showing 1 to 10 of 51 entries</div>
                                    </div>
                                    <div class="col-md-3">
                                    <nav aria-label="Page navigation example">
                                          <ul class="pagination justify-content-end`">
                                          <li class="page-item"><a class="page-link" href="#"><<</a></li>
                                          <li class="page-item"><a class="page-link" href="#"><</a></li>
                                          <li class="page-item"><a class="page-link" href="#">></a></li>
                                          <li class="page-item"><a class="page-link" href="#">>></a></li>
                                          </ul>
                                          </nav>
                                    </div>


                              </div>
                        </div>
                  </div>
            </div>



      </div>
</div>
</div>












</div>


<?php
include 'footer.php';
?>