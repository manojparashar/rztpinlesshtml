<?php
include 'header.php';
?>



<div class="main">
  <div class="p-4 mb-5">


    <div class="row">
      <div class="col-md-3 mb-4">
        <?php
        include 'left_activation_block.php';
        ?>
      </div>
      <div class="col-md-6 mb-4">
        <div class="text-center">
          <h3 class="title-color">Personalize Your Plan</h3>
        </div>
        <div class="w-75 mx-auto">
        <form>
            <div class="d-flex form-group justify-content-center">
              <label for="inputEmail3" class="col-form-label mb-5"><b>Zip Code:</b></label>
              <div class="ms-3 w-75">
                <input type="First" class="form-control" id="inputEmail3" placeholder="Enter the 5 digit zip">
              </div>
            </div>
          </form>
        </div>
        <hr>
        <div class="text-center">
          <h5 class="title-color py-3 mb-0 text-decoration-underline">Raza Mobiles Plans</h5>
        </div>
        <div class="container-xxl text-center plan-table">
          <div class="row my-3 mx-md-n5">
            <div class="col m-1 px-0"> </div>
            <div class="col m-1 px-0">
              01 MONTH
            </div>
            <div class="col m-1 px-0">
              03 MONTH
            </div>
            <div class="col m-1 px-0">
              06 MONTH
            </div>
            <div class="col m-1 px-0">
              12 MONTHS
            </div>
          </div>

          <div class="row align-items-center">
            <div class="col m-1 px-0">
              <p class="mb-0 fw-bold">250 MB</p>
              <p class="mb-0">5G+4G LTE</p>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$15.00</span> <br>Total: 15.00</button>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$13.00</span> <br>Total: 39.00</button>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$11.00</span> <br>Total: 66.00</button>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$10.00</span> <br>Total: 120.00</button>
            </div>
          </div>



          <div class="row align-items-center">
            <div class="col m-1 px-0">
              <p class="mb-0 fw-bold">2GB</p>
              <p class="mb-0">5G+4G LTE</p>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$19.00</span> <br>Total: 19.00</button>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$16.00</span> <br>Total: 48.00</button>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$15.00</span> <br>Total: 90.00</button>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$14.00</span> <br>Total: 168.00</button>
            </div>
          </div>



          <div class="row align-items-center">
            <div class="col m-1 px-0">
              <p class="mb-0 fw-bold">3GB</p>
              <p class="mb-0">5G+4G LTE</p>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$24.00</span> <br>Total: 24.00</button>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$22.00</span> <br>Total: 66.00</button>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$21.00</span> <br>Total: 126.00</button>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$20.00</span> <br>Total: 240.00</button>
            </div>
          </div>



          <div class="row align-items-center">
            <div class="col m-1 px-0">
              <p class="mb-0 fw-bold">6GB</p>
              <p class="mb-0">5G+4G LTE</p>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$29.00</span> <br>Total: 29.00</button>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$28.00</span> <br>Total: 84.00</button>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$27.00</span> <br>Total: 162.00</button>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$25.00</span> <br>Total: 120.00</button>
            </div>
          </div>



          <div class="row align-items-center">
            <div class="col m-1 px-0">
              <p class="mb-0 fw-bold">15GB</p>
              <p class="mb-0">5G+4G LTE</p>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$39.00</span> <br>Total: 39.00</button>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$36.00</span> <br>Total: 108.00</button>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$35.00</span> <br>Total: 210.00</button>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$30.00</span> <br>Total: 360.00</button>
            </div>
          </div>

          <div class="row align-items-center">
            <div class="col m-1 px-0">
              <p class="mb-0 fw-bold">Unlimited</p>
              <p class="mb-0">5G+4G LTE</p>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$49.00</span> <br>Total: 49.00</button>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$46.00</span> <br>Total: 138.00</button>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$45.00</span> <br>Total: 270.00</button>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">$40.00</span> <br>Total: 480.00</button>
            </div>
          </div>

        </div>

        <div class="container-xxl text-center py-5 plan-table">
          <div class="row">
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">Cost : $5.00</span> <br>Credit : $6.25</button>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">Cost : $10.00</span> <br>Credit : $12.50</button>
            </div>
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded planbutton" style="width: 100%;"><span class="mb-0 fw-bold">Cost : $20.00</span> <br>Credit : $25.00</button>
            </div>
          </div>

          <div class="text-center  py-2">
            <h5 class="title-color py-3 mb-0 text-decoration-underline">Global Unlimited</h5>
          </div>

          <div class="row">
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded " style="width: 100%;"><span class="mb-0 fw-bold">&nbsp;</button>
            </div>
          </div>
          <div class="text-center  py-2">
            <h5 class="title-color py-3 mb-0 text-decoration-underline">Global Unlimited</h5>
          </div>

          <div class="row">
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded" style="width: 100%;"><span class="mb-0 fw-bold">&nbsp;</button>
            </div>
          </div>
          <div class="row">
            <div class="col my-3">
              <hr>
            </div>
          </div>

          <div class="row">
            <div class="col m-1 px-0">
              <button id="activation" class="btn btn-secondary rounded" style="width: 100%;"><span class="mb-0 fw-bold">&nbsp;</button>
            </div>
          </div>
          <div class="row">
            <div class="col my-3">
              <hr>
            </div>
          </div>
          <div class="form-group row float-sm-end">
            <div class="col-sm-20">
              <button type="submit" class="btn btn-danger">Activate</button>
              <button type="submit" class="btn btn-danger">Port</button>
            </div>
          </div>


        </div>
      </div>

      <div class="col-md-3 mb-4">

        <div class="boxn mb-4">
          <h5 class="title-color">Activation Status</h5>
          <div class="row mb-2">
            <div class="col text-primary">
              ICCID:
            </div>
            <div class="col">
              8901260093179242194
            </div>
          </div>
          <div class="row mb-2">
            <div class="col text-primary">
              Activation Type:
            </div>
            <div class="col">
              NEW
            </div>
          </div>
          <div class="row mb-2">
            <div class="col text-primary">
              Phone Numner:
            </div>
            <div class="col">
              N/A
            </div>
          </div>
          <div class="row mb-2">
            <div class="col text-primary">
              Created:
            </div>
            <div class="col">
              02/21/2022 10:50 am
            </div>
          </div>
          <div class="row mb-2">
            <div class="col text-primary">
              Status:
            </div>
            <div class="col">
              Neutral
            </div>
          </div>
        </div>
      </div>


    </div>
  </div>

</div>


<?php
include 'footer.php';
?>