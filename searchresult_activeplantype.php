<?php
include 'header.php';
?>



<div class="main">

  <div class="p-4 mb-5">
    <div class="row">
      <div class="col-md-12 mb-4 search-details">
        <h3 class="title-color mb-0 "> Search Results For: <span>Active Plan Type | Last 24  hours</span> <a href="#">Clear</a></h3>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 mb-2">
        <div class="gbox1 ">
          <p class="text-semiBold">Search Results</p>
          <div class="card">
          <table class="table table-bordered table-responsive portin-table bg-white">
                                    <thead class="table-head-bg">
                                          <tr>
                                                <th>Number</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Plan</th>
                                                <th>Created</th>
                                                <th>Status</th>
                                                <th>Wallet</th>
                                                <th>Renewal Date</th>
                                                <th>Recharge Status</th>
                                               
                                          </tr>
                                    </thead>
                                    <tbody>
                                          <tr>
                                                <td><a href="#">(872) 806-8537</a></td>
                                                <td>ultra mobile</td>
                                                <td>ultrahottelecom@gmail.com</td>
                                                <td>$15</td>
                                                <td>03/16/22</td>
                                                <td>Active</td>
                                                <td>$0.00</td>
                                                <td>04/15/22</td>
                                                <td><a href="#">Pay Now</a></td>
                                                
                                          </tr>
                                          <tr>
                                          <td><a href="#">(872) 806-8537</a></td>
                                                <td>ultra mobile</td>
                                                <td>ultrahottelecom@gmail.com</td>
                                                <td>$15</td>
                                                <td>03/16/22</td>
                                                <td>Active</td>
                                                <td>$0.00</td>
                                                <td>04/15/22</td>
                                                <td><a href="#">Pay Now</a></td>
                                                
                                          </tr>
                                          <tr>
                                          <td><a href="#">(872) 806-8537</a></td>
                                                <td>ultra mobile</td>
                                                <td>ultrahottelecom@gmail.com</td>
                                                <td>$15</td>
                                                <td>03/16/22</td>
                                                <td>Active</td>
                                                <td>$0.00</td>
                                                <td>04/15/22</td>
                                                <td><a href="#">Pay Now</a></td>
                                                
                                          </tr>
                                          <tr>
                                          <td><a href="#">(872) 806-8537</a></td>
                                                <td>ultra mobile</td>
                                                <td>ultrahottelecom@gmail.com</td>
                                                <td>$15</td>
                                                <td>03/16/22</td>
                                                <td>Active</td>
                                                <td>$0.00</td>
                                                <td>04/15/22</td>
                                                <td><a href="#">Pay Now</a></td>
                                                
                                          </tr>
                                    </tbody>
                              </table>
                              <div class="row align-items-center">
                                   
                                    <div class="col-md-2">
                                          <div class="text-center">Showing 1 to 10 of 51 entries</div>
                                    </div>
                                    <div class="col-md-10">
                                          <nav aria-label="Page navigation example">
                                                <ul class="pagination justify-content-end">
                                                      <li class="page-item"><a class="page-link" href="#">
                                                                  <<< /a>
                                                      </li>
                                                      <li class="page-item"><a class="page-link" href="#">
                                                                  << /a>
                                                      </li>
                                                      <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                      <li class="page-item"><a class="page-link" href="#">></a></li>
                                                      <li class="page-item"><a class="page-link" href="#">>></a></li>
                                                </ul>
                                          </nav>
                                    </div>


                              </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
</div>
<?php
include 'footer.php';
?>