<?php
include 'header.php';
?>



<div class="main">
    <div class="p-4 mb-5">
    <h3 class="title-color py-3 mb-0">Edit My Company Profile :</h3>
    <hr>
    <div class="col-md-4 mb-4 text-success">
    Company Info
</div>



<div class="row fw-bold">
<div class=" col-md-4 mb-4">

 Company Name
 <div class="input-group mt-2">
  <input type="text" class="form-control" placeholder="Hot Telecom Devon Inc" aria-label="Recipient's username" aria-describedby="basic-addon2">
  
</div>
</div>


<div class="col-md-4 mb-4">

  Master Agent
  <div class="input-group mt-2">
  Mobilution International Incorporated (653)
 
</div>
</div>
</div>



<div class="row fw-bold">
<div class=" col-md-4 mb-4">

 Retaller Code
 <div class="input-group mt-2">
  <input type="text" class="form-control" placeholder="UMW10032" aria-label="Recipient's username" aria-describedby="basic-addon2">
  
</div>
</div>


<div class="col-md-4 mb-4">

  Parent Retaller Code:
  <div class="input-group mt-2">
 UMW10032 
 
</div>
</div>
</div>



<div class="row fw-bold">
<div class=" col-md-4 mb-4">

 Emida:
 <div class="input-group mt-2">
  <input type="text" class="form-control" placeholder="" aria-label="Recipient's username" aria-describedby="basic-addon2">
  
</div>
</div>
<div class="col-md-4 mb-4">

  ePay ID:
  <div class="input-group mt-2">
  <input type="text" class="form-control" placeholder= "" arial-label="Recipient's username" aria-describedby="basic-addon2">
 
</div>
</div>
</div>

<div class="col-md-4 mb-4 fw-bold">
    Current Status:
</div>


<div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
  <label class="form-check-label" for="flexCheckIndeterminate">
    Active
  </label>
  
</div>


<div class="col-md-4 my-4 text-success">
    Owner Contact Info
</div>



<div class="row fw-bold">
<div class=" col-md-4 mb-4">

 First Name
 <div class="input-group mt-2">
  <input type="text" class="form-control" placeholder="First Name" aria-label="Recipient's username" aria-describedby="basic-addon2">
  
</div>
</div>


<div class="col-md-4 mb-4">

  Last Name 
  <div class="input-group mt-2">
  <input type="text" class="form-control" placeholder="Last Name" aria-label="Recipient's username" aria-describedby="basic-addon2">
 
</div>
</div>
</div>



<div class="row fw-bold">
<div class=" col-md-4 mb-4">

 Phone:
 <div class="input-group mt-2">
  <input type="text" class="form-control" placeholder="(773)338-2228" aria-label="Recipient's username" aria-describedby="basic-addon2">
  
</div>
</div>


<div class="col-md-4 mb-4">

  Email:
  <div class="input-group mt-2">
  <input type="email" class="form-control" placeholder="mirza122007@gmail.com" aria-label="Recipient's username" aria-describedby="basic-addon2">
 
</div>
</div>
</div>



<div class="col-md-4 my-4 text-success">
    Main Address
</div>



<div class="row fw-bold">
<div class=" col-md-4 mb-4">

 Address:
 <div class="input-group mt-2">
  <input type="text" class="form-control" placeholder="2456-58 Devon Ave" aria-label="Recipient's username" aria-describedby="basic-addon2">
  
</div>
</div>


<div class="col-md-4 mb-4">

  Sulte #:
  <div class="input-group mt-2">
  <input type="text" class="form-control" placeholder="" aria-label="Recipient's username" aria-describedby="basic-addon2">
 
</div>
</div>
</div>



<div class="row fw-bold">
<div class=" col-md-4 mb-4">

 City:
 <div class="input-group mt-2">
  <input type="text" class="form-control" placeholder="Chicago" aria-label="Recipient's username" aria-describedby="basic-addon2">
  
</div>
</div>


<div class="col-md-4 mb-4">

  State:
  <div class="input-group mt-2">
  <input type="text" class="form-control" placeholder="IL" aria-label="Recipient's username" aria-describedby="basic-addon2">
 
</div>
</div>
</div>





<div class="row fw-bold">
<div class=" col-md-4 mb-4">

 Zip:
 <div class="input-group mt-2">
  <input type="text" class="form-control" placeholder="60659" aria-label="Recipient's username" aria-describedby="basic-addon2">
  
</div>
</div>


<div class="col-md-4 mb-4">
    <div class="input-group mt-2">
<div class="form-check">
  <input class="form-check-input" type="checkbox" value="" id="flexCheckIndeterminate">
  <label class="form-check-label" for="flexCheckIndeterminate">
    Check to be listed on the store locator
  </label>
</div>
</div>
  
  
</div>
</div>

<div>
<span class="btn btn-danger mt-4" id="">SAVE INFO</span>
</div>





    </div>
</div>
<?php
include 'footer.php';
?>