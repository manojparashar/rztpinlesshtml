<?php
include 'header.php';
?>



<div class="main">

  <div class="p-4 mb-5">
    <div class="row">
      <div class="col-md-12 mb-4 search-details">
        <h3 class="title-color mb-0 "> Manage Employees</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 mb-2">
        <div class="gbox1 ">
        <div class="row">
                        <div class="col-md-6">
                              <div class="row my-2">
                                        <div class="col-md-4 mb-3 mb-lg-0">
                                          <input type="text" placeholder="Filter by Name and Username" class="form-control">
                                    </div>
                                    <div class="col-md-4 mb-3 mb-lg-0">
                                          <select name="" id="" class="form-select">
                                                <option value="">Active Only</option>
                                                <option value="">Option 2</option>
                                                <option value="">Option 3</option>
                                          </select>
                                    </div>

                              </div>
                        </div>
                  </div>
          <div class="card mt-2">
          <table class="table table-bordered table-responsive portin-table bg-white">
                                    <thead class="table-head-bg">
                                          <tr>
                                                <th>Username</th>
                                                <th>Name</th>
                                                <th>User Status</th>
                                                <th>Access Level</th>
                                                <th>Edit Employee Profile</th>                                         
                                          </tr>
                                    </thead>
                                    <tbody>
                                          <tr>
                                                <td>hotdevon1</td>
                                                <td>Sami Khan</td>
                                                <td>Active</td>
                                                <td>Employee</td>
                                                <td><a href="#">Edit Employee Profile</a></td>

                                          </tr>
                                          
                                          
                                         
                                    </tbody>
                              </table>
                              <div class="row align-items-center">
                                   
                                    <div class="col-md-2">
                                          <div class="text-center">Showing 1 to 10 of 51 entries</div>
                                    </div>
                                    <div class="col-md-10">
                                          <nav aria-label="Page navigation example">
                                                <ul class="pagination justify-content-end">
                                                      <li class="page-item"><a class="page-link" href="#">
                                                                  <<< /a>
                                                      </li>
                                                      <li class="page-item"><a class="page-link" href="#">
                                                                  << /a>
                                                      </li>
                                                      <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                      <li class="page-item"><a class="page-link" href="#">></a></li>
                                                      <li class="page-item"><a class="page-link" href="#">>></a></li>
                                                </ul>
                                          </nav>
                                    </div>


                              </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
</div>
<?php
include 'footer.php';
?>